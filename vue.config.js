// eslint-disable-next-line @typescript-eslint/no-var-requires
const packageJson = require('./package.json');
process.env.VUE_APP_VERSION = packageJson.version;
process.env.VUE_APP_VUE_VERSION = packageJson.dependencies.vue;

let scssVariable = '';
for (let envKey in process.env) {
  if (/VUE_APP_/i.test(envKey)) {
    scssVariable += `$${envKey}: "${process.env[envKey]}";`;
    console.log('########', `$${envKey}: "${process.env[envKey]}";`);
  }
}

module.exports = {
  runtimeCompiler: true,
  css: {
    loaderOptions: {
      scss: {
        prependData: `
        ${scssVariable}
        @import "@/assets/scss/variable.scss";
        @import "@/assets/scss/global.scss";
        @import "@/assets/scss/importer.scss";
        `,
      },
    },
  },
  configureWebpack: {
    devtool: 'source-map',
  },
};
