import { collection, FirebaseFirestore, getFirestore, doc, setDoc, updateDoc, arrayUnion, where, query, getDocs } from 'firebase/firestore';
import { initializeApp } from 'firebase/app';
import { createUserWithEmailAndPassword, getAuth, Auth, User } from 'firebase/auth';

// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const config = {
  apiKey: 'AIzaSyA76YBnxGiY0ufiwPitLAs6r-qq7NX_mm8',
  authDomain: 'sabancidx-34.firebaseapp.com',
  databaseURL: 'https://sabancidx-34-default-rtdb.europe-west1.firebasedatabase.app',
  projectId: 'sabancidx-34',
  storageBucket: 'sabancidx-34.appspot.com',
  messagingSenderId: '256203338104',
  appId: '1:256203338104:web:0e81c2b283c6633a93588d',
  measurementId: 'G-X1VHP643VR',
};

export default class FirebaseConfig {
  private auth!: Auth;
  private db!: FirebaseFirestore;
  static _instance: FirebaseConfig;

  constructor() {
    if (FirebaseConfig._instance) {
      return FirebaseConfig._instance;
    }
    FirebaseConfig._instance = this;
    initializeApp(config); // 'firebase initialized';
    this.db = getFirestore();
    this.auth = getAuth();
  }

  signUp(email: string, password: string): void {
    createUserWithEmailAndPassword(this.auth, email, password)
      .then((userCredential: { user: any }) => {
        const user = userCredential.user;
        this.createUserMetaData(user);
      })
      .catch((error: { code: string; message: string }) => {
        const errorCode = error.code;
        const errorMessage = error.message;
        console.log(errorCode, errorMessage);
      });
  }

  createUserMetaData(user: User): void {
    try {
      const docRef = doc(this.db, 'users', user.uid); // or doc(collection(this.db, 'users'), user.uid);
      setDoc(docRef, {
        id: user.uid,
        first: 'Ada',
        last: 'Lovelace',
        born: 1815,
        date: [1998],
      });
    } catch (e) {
      console.error('Error adding document: ', e);
    }
  }

  /**
   * Update user meta information on firestore
   * @param user
   */
  updateUserMeta(user: User): void {
    updateDoc(doc(this.db, 'users', user.uid), {
      date: arrayUnion(19),
    });
  }

  async listUserMeta(user: User): Promise<void> {
    // Loop
    const q = query(collection(this.db, 'users'), where('id', '==', user.uid));
    const querySnapshot = await getDocs(q);
    querySnapshot.forEach((doc: any) => {
      // doc.data() is never undefined for query doc snapshots
      console.log(doc.id, ' => ', doc.data());
    });
  }
}
