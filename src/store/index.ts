import { createStore } from 'vuex';

export interface Store {
  products: Product[];
}

export interface Product {
  name: string;
  count: number;
}

export default createStore({
  state: {
    // state = $data
    products: [
      { name: 'domates', count: 13 },
      { name: 'patates', count: 34 },
      { name: 'patlıcan', count: 23 },
      { name: 'şeftali', count: 9 },
      { name: 'elma', count: 9 },
    ],
  },
  getters: {
    // getters = Computed value (state,)
    // this.$store.getters.getProducts
    getProducts(state: Store, getters) {
      console.warn(getters);
      return state.products;
    },
    availableProducts(state, getters) {
      console.assert(getters);
      const av = state.products.filter((item) => item.count > 13);
      console.log(av);
      return av;
    },
  },
  actions: {
    // fetch
  },
  mutations: {
    // setters = Methods(state, value)
    // this.$store.commit('addProduct', {name: 'kabak', count: 21})
    addProduct(state, value) {
      console.log(state, value);
      state.products.push(value);
    },
    setProducts(state, value) {
      console.log(value);
      state.products = value;
    },
  },
});
