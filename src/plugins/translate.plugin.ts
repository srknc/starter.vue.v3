import { App, DirectiveBinding, VNode } from 'vue';

const DxTranslate = {
  install: (app: App, options: { [key: string]: any }): any => {
    console.log(options);
    app.config.globalProperties.$translate = (key: string) => {
      console.log('gısmet');
      return (
        key.split('.').reduce((o, i) => {
          if (o) return o[i];
        }, options) || key
      );
    };

    app.provide('i18n', options);

    app.directive('my-directive', {
      mounted(el: HTMLElement, binding: DirectiveBinding, vnode: VNode): void {
        // some logic ...
        console.log('sadasd', el, binding, vnode);
      },
    });

    app.mixin({
      created() {
        // some logic ...
      },
    });
  },
};

export default DxTranslate;
