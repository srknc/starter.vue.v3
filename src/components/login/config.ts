import { reactive } from 'vue';
import { NavigationGuardNext, RouteLocationNormalized } from 'vue-router';

import router from '@/router';

router.beforeEach((to, from, next) => {
  loginStore.authentication(to, from, next);
});

declare module '@vue/runtime-core' {
  interface Vue {
    // or interface Vue {
    store: {
      login: LoginStore;
    };
  }
}

export interface LoginStore {
  isLogged: boolean;
  redirectTo?: RouteLocationNormalized;
  authentication: (to: RouteLocationNormalized, from: RouteLocationNormalized, next: NavigationGuardNext) => void;
}

// export const loginStore: LoginStore = {
//   isLogged: false,
//   username: '',
//   password: '',
//   authentication: (to: Route, from: Route, next: NavigationGuardNext<Vue>): void => {
//     if (to.meta.isRestricted && !loginStore.username) {
//       loginStore.redirectTo = to;
//       console.log('restricted page');
//       next({ path: 'login' });
//     } else {
//       next();
//     }
//   },
// };

export class LoginStoreConfig implements LoginStore {
  isLogged = false;
  redirectTo!: RouteLocationNormalized;

  authentication(to: RouteLocationNormalized, from: RouteLocationNormalized, next: NavigationGuardNext): void {
    if (to.matched.some((routePath) => routePath.meta.isRestricted) && !this.isLogged) {
      console.warn('restricted page access was blocked');
      this.redirectTo = to;
      next({ path: 'login' });
    } else {
      next();
    }
  }
}

export const loginStore = reactive(new LoginStoreConfig());
