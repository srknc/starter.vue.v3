interface LoadingStore {
  status: boolean;
  counter: number;
}

export default {
  namespaced: true,
  state: {
    status: false,
    counter: 0,
  },
  getters: {
    isLoading(state: LoadingStore): boolean {
      return state.status;
    },
    counterStatus(state: LoadingStore): number {
      return state.counter;
    },
  },
  mutations: {
    refreshStatus(state: LoadingStore, value: boolean): void {
      value ? state.counter++ : state.counter--;
      state.status = !!state.counter;
    },
  },
};
