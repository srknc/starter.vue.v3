export const currency = (val: number, currency: string = 'TRY', minimumFractionDigits: number = 0): string =>
  new Intl.NumberFormat('tr', { style: 'currency', currency, minimumFractionDigits }).format(val);
