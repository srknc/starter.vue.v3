import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';
import Home from '@/views/Home.vue';

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    props: {
      potato: 'It Comes From Route PROPS 2122',
    },
    beforeEnter(to, from, next) {
      const exist = true;
      exist ? next() : next({ name: 'NotFound' });
      // exist ? next() : next({ path: '/' }); // alternative with path declaration
      // exist ? next() : next('/'); // alternative with path shortened
      // if (!exist) {
      //   next({
      //     name: 'NotFound',
      //     params: { pathMatch: to.path.substring(1).split('/') },
      //   });
      // }
    },
  },
  {
    path: '/about',
    name: 'About',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '@/views/About.vue'),
    meta: {
      isRestricted: true,
    },
    props: { id: 'it comes from about routing => 1232' },
    children: [
      {
        path: ':id',
        name: 'Param',
        component: () => import(/* webpackChunkName: "about" */ '@/views/Param.vue'),
        props: true, // its gives shorthand access to id parameter from props: {id: string} as this.$route.params.id in component
      },
    ],
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import(/* webpackChunkName "login" */ '@/components/login/login.vue'),
  },
  {
    path: '/store',
    name: 'Store',
    component: () => import(/* webpackChunkName "login" */ '@/views/Store.vue'),
  },
  {
    path: '/demo',
    name: 'Demo',
    component: () => import(/* webpackChunkName "login" */ '@/views/Demo.vue'),
  },
  {
    path: '/form',
    name: 'FormBuilderPage',
    component: () => import(/* webpackChunkName "login" */ '@/views/FormBuilderPage.vue'),
  },
  {
    path: '/:pathMatch(.*)*',
    name: 'NotFound',
    component: () => import(/* webpackChunkName: "about" */ '@/components/not-found/not-found.component.vue'),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  linkActiveClass: 'active',
  linkExactActiveClass: 'sub-active',
  routes,
  scrollBehavior(to, from, savedPosition) {
    return savedPosition ? savedPosition : to.hash ? { el: to.hash, behavior: 'smooth' } : { top: 0, behavior: 'smooth' };

    // Or always scroll 10px above the element #main
    /**
    return {
      // could also be
      // el: document.getElementById('main'),
      el: '#main',
      top: -10,
    }*/

    // Or
    /**
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        resolve({ left: 0, top: 0 })
      }, 500)
    })*/
  },
});

export default router;
