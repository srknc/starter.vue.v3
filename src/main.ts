import { createApp } from 'vue';
import router from './router';
import store from './store';
import { filters } from './filters';
import App from './App.vue';
import DxTranslate from './plugins/translate.plugin';

const app = createApp(App);

// Auto Register Components Globally
const requireComponent = require.context('./components', true, /.*\.(vue|js)$/);
requireComponent.keys().forEach(function (fileName) {
  let baseComponentConfig = requireComponent(fileName);
  baseComponentConfig = baseComponentConfig.default || baseComponentConfig;
  const baseComponentName = baseComponentConfig.name || fileName.replace(/^.+\//, '').replace(/\.\w+$/, '');
  app.component(baseComponentName, baseComponentConfig);
});

// Global Error Handler
app.config.errorHandler = function (err: any, vm, info) {
  // handle error
  // `info` is a Vue-specific error info, e.g. which lifecycle hook
  // the error was found in. Only available in 2.2.0+
  console.warn(`Error occurred: ${err.toString()}\nInfo: ${info}`);
};

// Filters (or another name call Pipes)
app.config.globalProperties.$filters = { ...filters };

// Plugin
const i18nStrings = {
  greetings: {
    hi: 'Hallo!',
  },
};

app.use(store).use(router).use(DxTranslate, i18nStrings).mount('body');
