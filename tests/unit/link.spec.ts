import { shallowMount } from '@vue/test-utils';
import DxLink from '@/components/link/link.component.vue';

describe('DxLink Component', () => {
  it('renders props.msg when passed', () => {
    const to = 'new message';
    const wrapper = shallowMount(DxLink, {
      props: { to },
    });
    expect((wrapper as any).componentVM.linkProps(to)).toBe('router-link');
    expect((wrapper as any).componentVM.linkProps('http://www.google.com')).toBe('a');
  });
});
